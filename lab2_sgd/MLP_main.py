# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 20:24:56 2021

@author: AM4
"""
import pandas as pd
import numpy as np
from neural import Perceptron
import random


df = pd.read_csv('data.csv')

df = df.iloc[np.random.permutation(len(df))]
y = df.iloc[0:100, 4].values
y = np.where(y == "Iris-setosa", 1, 0).reshape(-1,1)
X = df.iloc[0:100, [0, 2]].values

inputSize = X.shape[1] # количество входных сигналов равно количеству признаков задачи 
hiddenSizes = 10 # задаем число нейронов скрытого (А) слоя 
outputSize = 1 if len(y.shape) else y.shape[1] # количество выходных сигналов равно количеству классов задачи

iterations = 50
learning_rate = 0.1

net = Perceptron(inputSize, hiddenSizes, outputSize)

# обучаем сеть (фактически сеть это вектор весов weights)
epoch = 10
for e in range(epoch):
    for i in range(iterations):
        dataIndex = random.randint(0, X.shape[0] - 1)
        x = X[dataIndex].reshape(1, 2)
        y_ = y[dataIndex].reshape(1, 1)
        net.train(x, y_, n_iter=1)

        if i % 10 == 0:
            print("На итерации: " + str(i) + ' || ' + "Средняя ошибка: " + str(np.mean(np.square(y - net.predict(X)[0]))))
            pass
        pass
    pass
pass

# считаем ошибку на обучающей выборке
pr = net.predict(X)[0]
print(sum(abs(y-(pr>0.5))))