import numpy as np
import pandas as pd

class Perceptron:
    def __init__(self, inputSize, hiddenSizes, outputSize):
        
        self.Win = np.zeros((1+inputSize,hiddenSizes))
        self.Win[0,:] = (np.random.randint(0, 3, size = (hiddenSizes)))
        self.Win[1:,:] = (np.random.randint(-1, 2, size = (inputSize,hiddenSizes)))
        
        self.Wout = np.random.randint(0, 2, size = (1+hiddenSizes,outputSize)).astype(np.float64)
        #self.Wout = np.random.randint(0, 3, size = (1+hiddenSizes,outputSize))
        
    def predict(self, Xp):
        hidden_predict = np.where((np.dot(Xp, self.Win[1:,:]) + self.Win[0,:]) >= 0.0, 1, -1).astype(np.float64)
        out = np.where((np.dot(hidden_predict, self.Wout[1:,:]) + self.Wout[0,:]) >= 0.0, 1, -1).astype(np.float64)
        return out, hidden_predict

    def train(self, X, y, eta=0.01):
        is_has_error = True
        is_same_error_vector = False
        last_weights = self.Wout.copy()
        error_list = []

        counter = 0



        while is_has_error and not is_same_error_vector:
            for xi, target, j in zip(X, y, range(X.shape[0])):
                pr, hidden = self.predict(xi)
                self.Wout[1:] += ((eta * (target - pr)) * hidden).reshape(-1, 1)
                self.Wout[0] += eta * (target - pr)

            is_same_error_vector = pd.DataFrame(last_weights[1:]).compare(pd.DataFrame(self.Wout[1:])).shape == (0, 0)
            last_weights = self.Wout.copy()
            is_has_error = self.Wout[0].all() != 0
            print(counter)
            counter+=1
            pass


        if not is_has_error:
            print("Ошибка ушла")
            pass

        if is_same_error_vector:
            print("Повторение вектора")
            pass

        return self
    pass
