# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 20:24:56 2021

@author: AM4
"""
import pandas as pd
import numpy as np
from neural import Perceptron


df = pd.read_csv('data.csv')

df = df.iloc[np.random.permutation(len(df))]
y = df.iloc[0:100, 4].values
y = np.where(y == "Iris-setosa", 1, -1)
X = df.iloc[0:100, [0, 2]].values


inputSize = X.shape[1] # количество входных сигналов равно количеству признаков задачи 
hiddenSizes = 10 # задаем число нейронов скрытого (А) слоя 
outputSize = 1 if len(y.shape) else y.shape[1] # количество выходных сигналов равно количеству классов задачи


NN = Perceptron(inputSize, hiddenSizes, outputSize)

NN.train(X, y, eta=0.00001)

y = df.iloc[:, 4].values
y = np.where(y == "Iris-setosa", 1, -1)
X = df.iloc[:, [0, 2]].values
out, hidden_predict = NN.predict(X)

print(sum(out-y.reshape(-1, 1)))

# print(y.reshape(-1, 1).flatten())
# print(out)
#
# data = {
#     "y": y.reshape(-1, 1).flatten(),
#     "out": out.flatten()
# }
#
# df = pd.DataFrame(data)
# print(df)

# print(y.reshape(-1, 1))
# print(out)


# [[0. 0. 1. 0. 0. 0. 1. 0. 1. 0. 1.]]
# [[ 0.    0.    1.   -0.04  0.    0.    1.    0.    1.    0.    1.  ]]
# [[ 0.    0.    1.   -0.04  0.    0.    1.    0.    1.    0.    1.  ]]
# [[ 0.    0.    1.   -0.04  0.    0.    1.    0.    1.    0.    1.  ]]
# [[ 0.    0.    1.   -0.04  0.    0.    1.    0.    1.    0.    1.  ]]

# [[0. 1. 0. 0. 0. 0. 0. 0. 0. 1. 1.]]
