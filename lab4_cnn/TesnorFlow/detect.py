import numpy as np
import tensorflow as tf
import cv2 as cv
from keras.src.preprocessing.image import ImageDataGenerator
from sklearn.metrics import accuracy_score

model = tf.keras.models.load_model('./model/model')

test_generator = ImageDataGenerator(rescale=1. / 255)
testDataset = test_generator.flow_from_directory(
    '../data/val',
    target_size=(512, 512),
    batch_size=1,
    class_mode='categorical',
    shuffle=False,
    classes=['cat', 'dog', 'wild']
)

result = model.predict(testDataset)
result = np.argmax(result, axis=1)
print(result)
print(testDataset.classes)
print(accuracy_score(testDataset.classes, result))
