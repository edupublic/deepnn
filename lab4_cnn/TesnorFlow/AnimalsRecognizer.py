import tensorflow as tf
from keras import Sequential
from keras.layers import *
import os
from Lenet5 import LeNet5

os.environ['MIOPEN_LOG_LEVEL'] = '4'
os.environ['ROCM_HOME'] = '/opt/rocm'
os.environ['ROCM_PATH'] = '/opt/rocm'

gpus = tf.config.list_physical_devices('GPU')
print(gpus)
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)

tf.config.run_functions_eagerly(True)

size = (512, 512, 3)

train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    rescale=1. / 255,
)

train_generator = train_datagen.flow_from_directory(
    '../data/train',
    target_size=(size[0], size[1]),
    batch_size=32,
    class_mode='categorical',
    classes=['cat', 'dog', 'wild']
)

validation_generator = train_datagen.flow_from_directory(
    '../data/val',
    target_size=(size[0], size[1]),
    batch_size=32,
    class_mode='categorical',
    classes=['cat', 'dog', 'wild']
)

model = LeNet5(shape=size, classes_count=3)

model.compile(
    optimizer=tf.keras.optimizers.legacy.Adam(learning_rate=1e-3),
    loss='categorical_crossentropy',
    metrics=['accuracy']
)

model.fit(
    train_generator,
    steps_per_epoch=50,
    batch_size=16,
    epochs=10,
    verbose=True,
    validation_data=validation_generator,
    validation_steps=50
)

model.save('./model/model')
