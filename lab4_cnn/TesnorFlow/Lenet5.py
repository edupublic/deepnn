from keras.layers import Conv2D, AveragePooling2D, Flatten, Dense
from keras.layers import concatenate
import tensorflow as tf


class LeNet5(tf.keras.Model):
    def __init__(self, classes_count, shape):
        super(LeNet5, self).__init__()
        # вернхняя половина
        self.c1f = Conv2D(6, 5, activation='tanh', input_shape=shape[1:])
        self.p1f = AveragePooling2D(2)
        self.c2f = Conv2D(16, 5, activation='tanh')
        # нижняя половина
        self.c1s = Conv2D(6, 5, activation='tanh', input_shape=shape[1:])
        self.p1s = AveragePooling2D(2)
        self.c2s = Conv2D(16, 5, activation='tanh')

        self.c3s = Conv2D(6, 5, activation='tanh', input_shape=shape[1:])
        self.p3s = AveragePooling2D(2)
        self.c4s = Conv2D(16, 5, activation='tanh')

        self.c5s = Conv2D(6, 5, activation='tanh', input_shape=shape[1:])
        self.p4s = AveragePooling2D(2)
        self.c6s = Conv2D(16, 5, activation='tanh')

        # полносвязная сеть
        self.f = Flatten()
        self.d1 = Dense(84, activation='tanh')
        self.d2 = Dense(classes_count, activation='softmax')
        pass

    def call(self, inputs, training=None, mask=None):
        xf = self.c1f(inputs)
        xf = self.p1f(xf)
        xf = self.c2f(xf)

        xs = self.c1s(inputs)
        xs = self.p1s(xs)
        xs = self.c2s(xs)

        x2s = self.c3s(inputs)
        x2s = self.p3s(x2s)
        x2s = self.c4s(x2s)

        x3s = self.c5s(inputs)
        x3s = self.p4s(x3s)
        x3s = self.c6s(x3s)

        x = concatenate([xf, xs, x2s, x3s], axis=-1)

        x = self.f(x)
        x = self.d1(x)
        x = self.d2(x)
        return x
